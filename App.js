import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/Componentes/home';
import SaibaMais from './src/Componentes/saibaMais';
import GameOver from './src/Componentes/gameOver'
import GameWin from './src/Componentes/gameWin'
import Nivel1 from './src/Fases/nivel1'
import Nivel2 from './src/Fases/nivel2'
import Nivel3 from './src/Fases/nivel3'
import Nivel4 from './src/Fases/nivel4'
import Nivel5 from './src/Fases/nivel5'
import Nivel6 from './src/Fases/nivel6'
import Nivel7 from './src/Fases/nivel7'
import Nivel8 from './src/Fases/nivel8'
import Nivel9 from './src/Fases/nivel9'
import Nivel10 from './src/Fases/nivel10'

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Home' screenOptions={{ headerShown: false }}>
        <Stack.Screen name='Home' component={HomeScreen} />
        <Stack.Screen name='SaibaMais' component={SaibaMais} />
        <Stack.Screen name='GameOver' component={GameOver} />
        <Stack.Screen name='Nivel1' component={Nivel1} />
        <Stack.Screen name='Nivel2' component={Nivel2} />
        <Stack.Screen name='Nivel3' component={Nivel3} />
        <Stack.Screen name='Nivel4' component={Nivel4} />
        <Stack.Screen name='Nivel5' component={Nivel5} />
        <Stack.Screen name='Nivel6' component={Nivel6} />
        <Stack.Screen name='Nivel7' component={Nivel7} />
        <Stack.Screen name='Nivel8' component={Nivel8} />
        <Stack.Screen name='Nivel9' component={Nivel9} />
        <Stack.Screen name='Nivel10' component={Nivel10} />
        <Stack.Screen name='GameWin' component={GameWin} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
