import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

const Level7 = ({ navigation }) => {
  const [phaseNumber] = useState(7);
  const [question] = useState("Qual era o nome original do Palmeiras?");
  
  const handleAnswer = (answer) => {
    if (answer === 'Palestra Itália') {
      navigation.navigate('Nivel8');
    } else {
      navigation.navigate('GameOver');
    }
  };
  
  return (
    <View style={styles.container}>
      <Text style={styles.phaseNumber}>{phaseNumber}</Text>
      <Text style={styles.question}>{question}</Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => handleAnswer('Sport Club Palmeiras')}>
          <Text style={styles.buttonText}>Sport Club Palmeiras</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => handleAnswer('Associação Atlética Palmeiras')}>
          <Text style={styles.buttonText}>Associação Atlética Palmeiras</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => handleAnswer('Palestra Itália')}>
          <Text style={styles.buttonText}>Palestra Itália</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => handleAnswer('Sociedade Esportiva Palmeiras')}>
          <Text style={styles.buttonText}>Sociedade Esportiva Palmeiras</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  phaseNumber: {
    position: 'absolute',
    top: 20,
    left: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
  question: {
    fontSize: 24,
    marginBottom: 20,
    textAlign: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#00C200',
    padding: 15,
    borderRadius: 10,
    width: '40%',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
  },
});

export default Level7;
