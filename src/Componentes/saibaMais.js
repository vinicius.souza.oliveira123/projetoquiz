import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 32,
        fontWeight: 'bold',
        marginBottom: 30,
        textAlign: 'center',
    },
    description: {
        fontSize: 20,
        textAlign: 'center',
    },
    button: {
        backgroundColor: 'green',
        padding: 15,
        borderRadius: 10,
        marginTop: 20,
        width: 80,
        alignItems: 'center'
    }
});

const LearnMoreScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Saiba Mais</Text>
            <Text style={styles.description}>
                Esse é um pequeno quiz sobre o time de futebol brasileiro Palmeiras feito com intuito exclusivamente para estudo
            </Text>
            <Text style={styles.description}>
                Espero que se divirta, bom jogo 😁
            </Text>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
                <Text style={{ color: 'white',  }}>
                    Voltar
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export default LearnMoreScreen;
