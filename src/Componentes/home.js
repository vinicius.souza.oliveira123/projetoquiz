import React from 'react';
import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from "@react-navigation/native";

const Home = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.centeredContent}>
        <View style={styles.logoContainer}>
          <Image source={require('../images/palmeiras.png')} style={styles.palmeirasLogo} />
          <Text style={styles.titleText}>Palmeiras Quiz</Text>
          <Image source={require('../images/palmeiras.png')} style={styles.palmeirasLogo} />
        </View>
        <TouchableOpacity style={[styles.button, styles.playButton]} onPress={() => navigation.navigate('Nivel1')}>
          <Text style={[styles.buttonText, styles.playButtonText]}>Jogar</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.button, styles.learnMoreButton]}
          onPress={() => navigation.navigate('SaibaMais')}>
          <Text style={[styles.buttonText, styles.learnMoreButtonText]}>Saiba Mais</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  centeredContent: {
    alignItems: 'center',
  },
  logoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  palmeirasLogo: {
    width: 120,
    height: 120,
    marginHorizontal: 10,
  },
  titleText: {
    fontSize: 40,
    fontWeight: 'bold',
    color: 'black',
    marginLeft: 10,
    marginRight: 10,
  },
  button: {
    padding: 15,
    borderRadius: 10,
    marginTop: 20,
  },
  playButton: {
    backgroundColor: '#ffd700',
  },
  learnMoreButton: {
    backgroundColor: '#00C200',
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  playButtonText: {
    color: '#000',
  },
  learnMoreButtonText: {
    color: '#fff',
  },
});

export default Home;
